# nuxt-experiment

I've had a few thoughts about Nuxt.js and [Slippers architecture discussions](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/752). 

[Hosted on GitLab Pages](https://tywilliams.gitlab.io/nuxt-experiment/)

## Performance notes

My experience with Nuxt is that it's a great static site generator, but there is some performance overhead. It's been a while since I last looked at a Nuxt.js [core web vitals](https://web.dev/vitals/) report, and I want to see what comes back with an off-the-shelf static build. 

## Module notes

The [Nuxt content module](https://content.nuxtjs.org/) is compelling. I haven't used it before and want to read more to evaluate. 

I've run into road bumps with the [Nuxt PWA module](https://pwa.nuxtjs.org/), although I think that was back when it was only the [community module](https://github.com/nuxt-community/pwa-module). Maybe things have changed. 

If we end up [choosing Tailwind](https://gitlab.slack.com/archives/C01CZB21WU9/p1607039732016900), there's also a [Tailwind CSS module](https://tailwindcss.nuxtjs.org/).

I've installed these modules to take a closer look under the hood. They're exciting, but I want to inspect them more closely before arriving at any conclusions. 

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
